package com.dkostyrev.itunesbrowser

import android.app.Application
import androidx.compose.ui.ExperimentalComposeUiApi
import com.dkostyrev.itunesbrowser.api.itunesApiModule
import com.dkostyrev.itunesbrowser.dao.itemDatabaseModule
import com.dkostyrev.itunesbrowser.json.moshiModule
import com.dkostyrev.itunesbrowser.main.mainActivityModule
import com.dkostyrev.itunesbrowser.repository.itemsRepositoryModule
import com.dkostyrev.itunesbrowser.util.DefaultSchedulers
import com.dkostyrev.itunesbrowser.util.Schedulers
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

@OptIn(ExperimentalComposeUiApi::class)
class ItunesBrowserApplication : Application() {

  override fun onCreate() {
    super.onCreate()

    startKoin {
      androidLogger(Level.ERROR)
      androidContext(this@ItunesBrowserApplication)

      modules(
        defaultModule,
        moshiModule,
        itunesApiModule,
        itemDatabaseModule,
        itemsRepositoryModule,
        mainActivityModule,
      )
    }
  }

  private val defaultModule = module {
    single<Schedulers> { DefaultSchedulers() }
  }
}
