package com.dkostyrev.itunesbrowser.api

import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.domain.Result
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ItunesApi {

  @GET("search")
  fun search(
    @Query("term") query: String,
    @Query("media") mediaType: String,
    @Query("entity") entity: String,
    @Query("limit") limit: Int = 50
  ): Single<Result<Item>>

}
