package com.dkostyrev.itunesbrowser.api

import com.dkostyrev.itunesbrowser.util.LoggingInterceptor
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit

class ItunesApiFactory {

  fun create(
    url: String = "https://itunes.apple.com",
    moshi: Moshi,
  ): ItunesApi {
    return Retrofit.Builder()
      .addConverterFactory(MoshiConverterFactory.create(moshi))
      .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
      .client(
        OkHttpClient().newBuilder()
          .connectTimeout(60, TimeUnit.SECONDS)
          .readTimeout(60, TimeUnit.SECONDS)
          .writeTimeout(60, TimeUnit.SECONDS)
          .addInterceptor(LoggingInterceptor("ItunesApi"))
          .build()
      )
      .baseUrl(url)
      .build()
      .create()
  }
}
