package com.dkostyrev.itunesbrowser.api

import org.koin.dsl.module

val itunesApiModule = module {
  single { ItunesApiFactory() }
  single { get<ItunesApiFactory>().create(moshi = get()) }
}
