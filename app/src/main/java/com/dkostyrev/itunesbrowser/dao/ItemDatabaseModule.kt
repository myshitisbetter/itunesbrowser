package com.dkostyrev.itunesbrowser.dao

import androidx.room.Room
import org.koin.dsl.module

val itemDatabaseModule = module {
  single {
    Room.databaseBuilder(get(), ItunesDatabase::class.java, "items").build()
  }

  single {
    get<ItunesDatabase>().itunesDao()
  }
}
