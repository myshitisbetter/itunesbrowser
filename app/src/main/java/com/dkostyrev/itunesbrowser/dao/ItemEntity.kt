package com.dkostyrev.itunesbrowser.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "items")
data class ItemEntity(
  @PrimaryKey(autoGenerate = true) val id: Int = 0,
  @ColumnInfo(name = "queryId") val queryId: Int,
  @ColumnInfo(name = "trackId") val trackId: String,
  @ColumnInfo(name = "trackName") val trackName: String,
  @ColumnInfo(name = "shortDescription") val shortDescription: String?,
  @ColumnInfo(name = "longDescription") val longDescription: String?,
  @ColumnInfo(name = "artwork") val artwork: String?,
)
