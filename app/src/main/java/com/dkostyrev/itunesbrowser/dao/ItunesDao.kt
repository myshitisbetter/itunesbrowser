package com.dkostyrev.itunesbrowser.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

@Dao
interface ItunesDao {

  @Query("SELECT * FROM queries")
  fun queries(): Single<List<QueryEntity>>

  @Query("SELECT * FROM queries WHERE term == :term AND media == :media AND entity == :entity")
  fun queryByTerm(term: String, media: String, entity: String): Maybe<QueryEntity>

  @Query("SELECT * FROM items WHERE queryId == :queryId")
  fun itemsByQueryId(queryId: Int): Single<List<ItemEntity>>

  @Query("DELETE FROM items WHERE queryId == :queryId")
  fun deleteByQueryId(queryId: Int)

  @Insert
  fun insertQuery(query: QueryEntity): Long

  @Insert
  fun insertItems(items: List<ItemEntity>)
}
