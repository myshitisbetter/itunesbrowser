package com.dkostyrev.itunesbrowser.dao

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ItemEntity::class, QueryEntity::class], version = 1, exportSchema = false)
abstract class ItunesDatabase : RoomDatabase() {
  abstract fun itunesDao(): ItunesDao
}
