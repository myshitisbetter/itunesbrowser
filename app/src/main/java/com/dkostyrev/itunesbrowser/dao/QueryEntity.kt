package com.dkostyrev.itunesbrowser.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "queries")
data class QueryEntity(
  @PrimaryKey(autoGenerate = true) val id: Int = 0,
  @ColumnInfo(name = "term") val term: String,
  @ColumnInfo(name = "media") val media: String,
  @ColumnInfo(name = "entity") val entity: String,
)
