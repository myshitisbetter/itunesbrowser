package com.dkostyrev.itunesbrowser.details

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.dkostyrev.itunesbrowser.ui.Image
import com.dkostyrev.itunesbrowser.ui.theme.Typography

@Composable
fun DetailsScreen(
  state: DetailsScreenState,
  scaffoldState: ScaffoldState = rememberScaffoldState(),
  onBackPressed: () -> Unit,
) {

  BackHandler(onBack = onBackPressed)
  Scaffold(
    scaffoldState = scaffoldState,
    topBar = {
      DetailsScreenAppBar(
        title = state.item.trackName,
        onBackPressed = onBackPressed
      )
    }
  ) {
    Row(modifier = Modifier.fillMaxSize().padding(16.dp)) {
      Image(state.item.artwork, Modifier.size(60.dp, 80.dp))

      Spacer(Modifier.width(16.dp))

      Column {
        if (state.item.longDescription != null) {
          Text(state.item.longDescription, style = Typography.body1)
        }
      }
    }
  }
}
