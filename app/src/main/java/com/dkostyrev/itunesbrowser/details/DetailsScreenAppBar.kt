package com.dkostyrev.itunesbrowser.details

import androidx.compose.foundation.layout.Box
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import com.dkostyrev.itunesbrowser.R
import com.dkostyrev.itunesbrowser.ui.theme.Purple80

@Composable
fun DetailsScreenAppBar(
  title: String,
  modifier: Modifier = Modifier,
  onBackPressed: () -> Unit
) {
  TopAppBar(
    backgroundColor = Purple80,
    modifier = modifier,
    title = {
      Box(contentAlignment = Alignment.CenterStart) {
        Text(
          text = title,
          style = TextStyle.Default.copy(color = Color.DarkGray),
          overflow = TextOverflow.Ellipsis,
          maxLines = 1,
        )
      }
    },
    navigationIcon = {
      IconButton(
        content = {
          Icon(
            imageVector = Icons.Filled.ArrowBack,
            contentDescription = stringResource(R.string.back)
          )
        },
        onClick = {
          onBackPressed()
        },
      )
    }
  )
}
