package com.dkostyrev.itunesbrowser.details

import com.dkostyrev.itunesbrowser.domain.Item

data class DetailsScreenState(val item: Item)
