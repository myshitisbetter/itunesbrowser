package com.dkostyrev.itunesbrowser.domain

import android.net.Uri
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Item(
  @Json(name = "trackId") val trackId: String,
  @Json(name = "trackName") val trackName: String,
  @Json(name = "shortDescription") val shortDescription: String?,
  @Json(name = "longDescription") val longDescription: String?,
  @Json(name = "artworkUrl100") val artwork: Uri?,
)
