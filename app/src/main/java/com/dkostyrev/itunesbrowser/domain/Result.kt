package com.dkostyrev.itunesbrowser.domain

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Result<T : Any>(
  @Json(name = "results") val results: List<T>,
  @Json(name = "resultCount") val count: Int,
)
