package com.dkostyrev.itunesbrowser.json

import com.squareup.moshi.Moshi

class MoshiFactory {

  fun create(): Moshi {
    return Moshi.Builder()
      .add(UriJsonAdapter)
      .build()
  }
}
