package com.dkostyrev.itunesbrowser.json

import org.koin.dsl.module

val moshiModule = module {
  single { MoshiFactory() }
  single { get<MoshiFactory>().create() }
}
