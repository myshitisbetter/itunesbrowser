package com.dkostyrev.itunesbrowser.json

import android.net.Uri
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson

object UriJsonAdapter : JsonAdapter<Uri>() {

  @FromJson
  override fun fromJson(reader: JsonReader): Uri? {
    return if (reader.peek() == JsonReader.Token.STRING) {
      Uri.parse(reader.nextString())
    } else {
      null
    }
  }

  @ToJson
  override fun toJson(writer: JsonWriter, value: Uri?) {
    if (value != null) {
      writer.value(value.toString())
    } else {
      writer.nullValue()
    }
  }
}
