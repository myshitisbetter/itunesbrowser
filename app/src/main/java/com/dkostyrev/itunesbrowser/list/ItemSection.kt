package com.dkostyrev.itunesbrowser.list

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.ui.Image
import com.dkostyrev.itunesbrowser.ui.theme.Typography

@Composable
fun ItemSection(
  item: Item,
  onClick: (Item) -> Unit,
  modifier: Modifier = Modifier,
) {
  Box(
    modifier = Modifier.clickable { onClick(item) }.then(modifier)
  ) {
    Row(
      modifier = Modifier.padding(16.dp).fillMaxWidth()
    ) {
      Image(item.artwork, Modifier.size(60.dp, 80.dp))

      Spacer(Modifier.width(16.dp))

      Column(modifier = Modifier.fillMaxWidth()) {
        Text(item.trackName, style = Typography.subtitle1)

        (item.shortDescription ?: item.longDescription)?.let {
          Text(it, style = Typography.subtitle2, maxLines = 2, overflow = TextOverflow.Ellipsis)
        }
      }
    }
  }
}
