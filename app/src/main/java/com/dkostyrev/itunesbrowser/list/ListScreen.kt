package com.dkostyrev.itunesbrowser.list

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.dkostyrev.itunesbrowser.R
import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.ui.theme.Typography

@ExperimentalComposeUiApi
@Composable
fun ListScreen(
  state: ListScreenState,
  scaffoldState: ScaffoldState = rememberScaffoldState(),
  onQuerySubmitted: (String) -> Unit,
  onItemSelected: (Item) -> Unit
) {

  val keyboardController = LocalSoftwareKeyboardController.current
  val focusManager = LocalFocusManager.current

  Scaffold(
    scaffoldState = scaffoldState,
    topBar = {
      ListScreenAppBar(
        query = state.query.orEmpty()
      ) {
        focusManager.clearFocus()
        keyboardController?.hide()
        onQuerySubmitted(it)
      }
    }
  ) {
    when {
      state.loading -> {
        Column(
          modifier = Modifier.fillMaxSize(),
          verticalArrangement = Arrangement.Center,
          horizontalAlignment = Alignment.CenterHorizontally
        ) {
          CircularProgressIndicator()
        }
      }

      state.showRetry -> {
        Column(
          modifier = Modifier.fillMaxSize(),
          verticalArrangement = Arrangement.Center,
          horizontalAlignment = Alignment.CenterHorizontally
        ) {

          Text(
            text = stringResource(R.string.error),
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(16.dp),
            style = Typography.body2
          )

          Button(onClick = { onQuerySubmitted(state.query.orEmpty()) }) {
            Text(stringResource(R.string.retry))
          }
        }
      }

      else -> {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
          items(state.results) {
            ItemSection(it, onItemSelected)
          }
        }
      }
    }
  }
}

