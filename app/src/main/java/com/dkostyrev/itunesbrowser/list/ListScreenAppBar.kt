package com.dkostyrev.itunesbrowser.list

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import com.dkostyrev.itunesbrowser.R
import com.dkostyrev.itunesbrowser.ui.theme.Purple80

@Composable
fun ListScreenAppBar(
  query: String,
  modifier: Modifier = Modifier,
  onQuerySubmitted: (String) -> Unit
) {
  val queryState = rememberSaveable(stateSaver = TextFieldValue.Saver) { mutableStateOf(TextFieldValue(text = query)) }

  fun submitQuery(query: String = queryState.value.text) {
    queryState.value = queryState.value.copy(text = query)
    onQuerySubmitted(query)
  }

  TopAppBar(
    backgroundColor = Purple80,
    modifier = modifier,
    title = {
      Box(contentAlignment = Alignment.CenterStart) {
        BasicTextField(
          modifier = Modifier.fillMaxWidth(),
          value = queryState.value,
          keyboardOptions = KeyboardOptions.Default.copy(
            imeAction = ImeAction.Search
          ),
          keyboardActions = KeyboardActions(onSearch = {
            submitQuery()
          }),
          onValueChange = { queryState.value = it },
          singleLine = true,
        )
        if (queryState.value.text.isEmpty()) {
          Text(
            text = stringResource(R.string.hint),
            style = TextStyle.Default.copy(color = Color.DarkGray)
          )
        }
      }

    },
    actions = {
      if (queryState.value.text.isNotEmpty()) {
        IconButton(
          content = {
            Icon(
              imageVector = Icons.Filled.Clear,
              contentDescription = stringResource(R.string.clear)
            )
          },
          onClick = {
            submitQuery("")
          }
        )
      }

      IconButton(
        enabled = queryState.value.text.isNotEmpty(),
        content = {
          Icon(
            imageVector = Icons.Filled.Search,
            contentDescription = stringResource(R.string.search)
          )
        },
        onClick = {
          submitQuery()
        }
      )
    }
  )
}
