package com.dkostyrev.itunesbrowser.list

import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.repository.ItemsRepository
import com.dkostyrev.itunesbrowser.util.LoadingState
import com.kostyrev.redux.Middleware
import com.kostyrev.redux.Reducer
import com.kostyrev.redux.Store
import com.kostyrev.redux.SubscriptionAwareStore
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.kotlin.cast

class ListScreenStore(
  private val repository: ItemsRepository,
) : Store<ListScreenState, ListScreenAction, ListScreenEffect> by SubscriptionAwareStore(
  reducer = ListScreenReducer(),
  middleware = listOf(
    LoadingMiddleware(repository)
  ),
  initialState = ListScreenState()
) {

  private class ListScreenReducer : Reducer<ListScreenState, ListScreenEffect> {
    override fun reduce(state: ListScreenState, effect: ListScreenEffect): ListScreenState {
      return when (effect) {
        is ListScreenEffect.QueryChanged -> {
          when (effect.state) {
            is LoadingState.Error -> state.copy(
              query = effect.query,
              loading = false,
              showRetry = true,
            )
            is LoadingState.Loaded -> state.copy(
              query = effect.query,
              loading = false,
              showRetry = false,
              results = effect.state.data,
            )
            LoadingState.Loading -> state.copy(
              query = effect.query,
              loading = true,
              showRetry = false,
            )
          }
        }
      }
    }
  }

  private class LoadingMiddleware(
    private val repository: ItemsRepository
  ) : Middleware<ListScreenState, ListScreenAction, ListScreenEffect> {
    @Suppress("REDUNDANT_ELSE_IN_WHEN")
    override fun create(action: ListScreenAction, state: ListScreenState): Observable<ListScreenEffect>? {
      return when (action) {
        is ListScreenAction.SubmitQuery -> {
          repository.search(action.query, mediaType = "movie", entity = "movie")
            .toObservable()
            .map { LoadingState.Loaded(it) }
            .cast<LoadingState<List<Item>>>()
            .startWithItem(LoadingState.Loading)
            .onErrorReturn { LoadingState.Error(it) }
            .map { ListScreenEffect.QueryChanged(action.query, it) }
            .cast<ListScreenEffect>()
            .onErrorComplete()
        }
        else -> null
      }
    }
  }
}

data class ListScreenState(
  val loading: Boolean = false,
  val showRetry: Boolean = false,
  val query: String? = null,
  val results: List<Item> = emptyList(),
)

sealed class ListScreenAction {
  data class SubmitQuery(val query: String) : ListScreenAction()
}

sealed class ListScreenEffect {
  data class QueryChanged(val query: String, val state: LoadingState<List<Item>>) : ListScreenEffect()
}

