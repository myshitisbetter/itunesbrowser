package com.dkostyrev.itunesbrowser.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.ComposeView
import com.dkostyrev.itunesbrowser.util.bindToLifecycle
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.activityScope
import org.koin.core.scope.Scope

@ExperimentalComposeUiApi
class MainActivity : ComponentActivity(), AndroidScopeComponent {

  override val scope: Scope by activityScope()
  private val store: MainScreenStore by scope.inject()

  init {
    lifecycle.bindToLifecycle(onAcquire = { store.subscribe() }, onRelease = { it.dispose() })
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(ComposeView(this).apply {
      setContent {
        MainScreen(store)
      }
    })
  }
}
