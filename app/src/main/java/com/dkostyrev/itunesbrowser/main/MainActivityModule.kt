package com.dkostyrev.itunesbrowser.main

import androidx.compose.ui.ExperimentalComposeUiApi
import com.dkostyrev.itunesbrowser.list.ListScreenStore
import org.koin.dsl.module

@ExperimentalComposeUiApi
val mainActivityModule = module {
  scope<MainActivity> {
    scoped { ListScreenStore(get()) }
    scoped { MainScreenStore(get()) }
  }
}
