package com.dkostyrev.itunesbrowser.main

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rxjava3.subscribeAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import com.dkostyrev.itunesbrowser.details.DetailsScreen
import com.dkostyrev.itunesbrowser.list.ListScreen
import com.dkostyrev.itunesbrowser.list.ListScreenAction
import com.dkostyrev.itunesbrowser.ui.theme.ItunesBrowserTheme

@Composable
@ExperimentalComposeUiApi
fun MainScreen(store: MainScreenStore) {
  ItunesBrowserTheme {
    Surface(
      modifier = Modifier.fillMaxSize(),
      color = MaterialTheme.colors.background,
    ) {
      val state = store.stateChanges().subscribeAsState(MainScreenState())

      when (val screen = state.value.currentScreen) {
        is ItunesBrowserScreen.ListScreen -> {
          ListScreen(
            state = screen.state,
            onQuerySubmitted = {
              store.listScreenStore.dispatch(ListScreenAction.SubmitQuery(it))
            },
            onItemSelected = {
              store.dispatch(MainScreenAction.OpenDetails(it))
            }
          )
        }
        is ItunesBrowserScreen.DetailsScreen -> {
          DetailsScreen(
            state = screen.state,
            onBackPressed = {
              store.dispatch(MainScreenAction.OpenList)
            }
          )
        }
      }
    }
  }
}
