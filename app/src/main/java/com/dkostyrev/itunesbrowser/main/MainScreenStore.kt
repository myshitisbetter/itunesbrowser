package com.dkostyrev.itunesbrowser.main

import com.dkostyrev.itunesbrowser.details.DetailsScreenState
import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.list.ListScreenState
import com.dkostyrev.itunesbrowser.list.ListScreenStore
import com.kostyrev.redux.Store
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.BehaviorSubject

class MainScreenStore(
  val listScreenStore: ListScreenStore,
) : Store<MainScreenState, MainScreenAction, Unit> {
  private val states = BehaviorSubject.createDefault(MainScreenState())

  override fun getState(): MainScreenState {
    return states.value!!
  }

  override fun stateChanges(): Observable<MainScreenState> {
    return states.hide()
  }

  override fun dispatch(action: MainScreenAction) {
    mutateState { state ->
      when (action) {
        is MainScreenAction.OpenList -> {
          state.copy(currentScreen = ItunesBrowserScreen.ListScreen(listScreenStore.getState()))
        }

        is MainScreenAction.OpenDetails -> {
          state.copy(currentScreen = ItunesBrowserScreen.DetailsScreen(DetailsScreenState(action.item)))
        }
      }
    }
  }

  private inline fun mutateState(mutator: (MainScreenState) -> MainScreenState) {
    val previousState = getState()
    val nextState = mutator(previousState)
    if (nextState != previousState) {
      states.onNext(nextState)
    }
  }

  override fun subscribe(): Disposable {
    return CompositeDisposable().apply {
      add(subscribeToListScreenStateChanges())
      add(listScreenStore.subscribe())
    }
  }

  private fun subscribeToListScreenStateChanges(): Disposable {
    return listScreenStore.stateChanges().subscribe { state ->
      mutateState {
        if (it.currentScreen is ItunesBrowserScreen.ListScreen) {
          it.copy(currentScreen = it.currentScreen.copy(state))
        } else {
          it
        }
      }
    }
  }
}

sealed class MainScreenAction {
  object OpenList : MainScreenAction()
  data class OpenDetails(val item: Item) : MainScreenAction()
}

data class MainScreenState(
  val currentScreen: ItunesBrowserScreen = ItunesBrowserScreen.ListScreen(ListScreenState())
)

sealed class ItunesBrowserScreen {
  data class ListScreen(val state: ListScreenState) : ItunesBrowserScreen()
  data class DetailsScreen(val state: DetailsScreenState) : ItunesBrowserScreen()
}
