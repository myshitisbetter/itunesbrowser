package com.dkostyrev.itunesbrowser.repository

import android.net.Uri
import com.dkostyrev.itunesbrowser.api.ItunesApi
import com.dkostyrev.itunesbrowser.dao.ItemEntity
import com.dkostyrev.itunesbrowser.dao.ItunesDao
import com.dkostyrev.itunesbrowser.dao.QueryEntity
import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.util.Schedulers
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

class ItemsRepository(
  private val api: ItunesApi,
  private val dao: ItunesDao,
  private val schedulers: Schedulers,
) {

  fun search(query: String, mediaType: String, entity: String): Single<List<Item>> {
    return Single.defer {
      if (query.isEmpty()) {
        return@defer Single.just(emptyList())
      }

      loadItemsFromDao(query, mediaType, entity)
        .switchIfEmpty(
          api.search(query, mediaType = "movie", entity = "movie", limit = 50)
            .observeOn(schedulers.io)
            .map { it.results }
            .doOnSuccess {
              val cachedQueryId = dao.insertQuery(QueryEntity(term = query, media = mediaType, entity = entity))
              dao.insertItems(it.map { it.toItemEntity(cachedQueryId) })
            }
        )
        .subscribeOn(schedulers.io)
        .observeOn(schedulers.mainThread)
    }
  }

  private fun loadItemsFromDao(query: String, media: String, entity: String): Maybe<List<Item>> {
    return dao.queryByTerm(query, media, entity)
      .flatMap { cachedQuery ->
        dao.itemsByQueryId(cachedQuery.id)
          .toMaybe()
          .map { result ->
            result.map { it.toItem() }
          }
          .flatMap {
            if (it.isEmpty()) Maybe.empty() else Maybe.just(it)
          }
      }
      .subscribeOn(schedulers.io)
      .observeOn(schedulers.io)
  }

  private fun Item.toItemEntity(queryId: Long): ItemEntity {
    return ItemEntity(
      queryId = queryId.toInt(),
      trackId = trackId,
      trackName = trackName,
      shortDescription = shortDescription,
      longDescription = longDescription,
      artwork = artwork?.toString()
    )
  }

  private fun ItemEntity.toItem(): Item {
    return Item(
      trackId = trackId,
      trackName = trackName,
      shortDescription = shortDescription,
      longDescription = longDescription,
      artwork = Uri.parse(artwork)
    )
  }
}
