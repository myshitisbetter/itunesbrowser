package com.dkostyrev.itunesbrowser.repository

import org.koin.dsl.module

val itemsRepositoryModule = module {
  single { ItemsRepository(get(), get(), get()) }
}
