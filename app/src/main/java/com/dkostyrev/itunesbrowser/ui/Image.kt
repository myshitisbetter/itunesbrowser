package com.dkostyrev.itunesbrowser.ui

import android.net.Uri
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import com.dkostyrev.itunesbrowser.ui.theme.Purple80
import com.dkostyrev.itunesbrowser.ui.theme.PurpleGrey40
import com.skydoves.landscapist.ShimmerParams
import com.skydoves.landscapist.glide.GlideImage

@Composable
fun Image(url: Uri?, modifier: Modifier = Modifier) {
  GlideImage(
    imageModel = url,
    contentScale = ContentScale.Fit,
    modifier = modifier,
    shimmerParams = ShimmerParams(
      baseColor = PurpleGrey40,
      highlightColor = Purple80,
    ),
  )
}
