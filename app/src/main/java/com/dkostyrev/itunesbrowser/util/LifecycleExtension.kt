package com.dkostyrev.itunesbrowser.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

fun <T> Lifecycle.bindToLifecycle(
  onAcquire: () -> T,
  acquireEvent: Lifecycle.Event = Lifecycle.Event.ON_CREATE,
  onRelease: (T) -> Unit,
  releaseEvent: Lifecycle.Event = Lifecycle.Event.ON_DESTROY,
) {
  addObserver(object : LifecycleEventObserver {
    private var value: T? = null
    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
      when (event) {
        acquireEvent -> {
          require(value == null) { "Value was not released" }
          value = onAcquire()
        }
        releaseEvent -> {
          require(value != null) { "Value was not acquired" }
          value?.let(onRelease)
          value = null
        }
        else -> Unit
      }
    }
  })
}
