package com.dkostyrev.itunesbrowser.util

sealed class LoadingState<out T : Any> {
  object Loading : LoadingState<Nothing>()
  data class Loaded<T : Any>(val data: T) : LoadingState<T>()
  data class Error(val error: Throwable) : LoadingState<Nothing>()
}
