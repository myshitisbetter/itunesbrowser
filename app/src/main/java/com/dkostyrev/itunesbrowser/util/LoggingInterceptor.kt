package com.dkostyrev.itunesbrowser.util

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

internal class LoggingInterceptor(private val tag: String) : Interceptor {

  override fun intercept(chain: Interceptor.Chain): Response {
    val request = chain.request().also {
      Log.i(tag, "-> ${it.method} ${it.url}")
    }
    return chain.proceed(request).also {
      Log.i(tag, "<- ${request.method} ${request.url} ${it.code}")
    }
  }
}
