package com.dkostyrev.itunesbrowser.util

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers as RxSchedulers

interface Schedulers {
  val io: Scheduler
  val computation: Scheduler
  val mainThread: Scheduler
}

class DefaultSchedulers : Schedulers {
  override val io: Scheduler
    get() = RxSchedulers.io()

  override val computation: Scheduler
    get() = RxSchedulers.computation()

  override val mainThread: Scheduler
    get() = AndroidSchedulers.mainThread()
}
