package com.dkostyrev.itunesbrowser.list

import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.repository.ItemsRepository
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.SingleSubject
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException

class ListScreenStoreTest {

  private val repository = mockk<ItemsRepository>()
  private val store = ListScreenStore(repository)

  private var disposable: Disposable? = null

  @Before
  fun setUp() {
    disposable = store.subscribe()
  }

  @After
  fun tearDown() {
    disposable?.dispose()
  }

  @Test
  fun `stateChanges - emits default state`() {
    val observer = store.stateChanges().test()

    observer.assertValueCount(1)
    observer.assertValue(ListScreenState())
  }

  @Test
  fun `SubmitQuery - changes state to loading - loading pending`() {
    val subject = SingleSubject.create<List<Item>>()
    givenSearchResult("test", subject)
    val observer = store.stateChanges().test()

    store.dispatch(ListScreenAction.SubmitQuery("test"))

    observer.assertValueCount(2)
    observer.assertValueAt(1, ListScreenState(loading = true, query = "test"))
  }

  @Test
  fun `SubmitQuery - sets result - loading completed`() {
    val subject = SingleSubject.create<List<Item>>()
    givenSearchResult("test", subject)
    val observer = store.stateChanges().test()
    store.dispatch(ListScreenAction.SubmitQuery("test"))
    val results = listOf(createItem())

    subject.onSuccess(results)

    observer.assertValueCount(3)
    observer.assertValueAt(2, ListScreenState(loading = false, query = "test", results = results))
  }

  @Test
  fun `SubmitQuery - sets show retry - loading failed`() {
    val subject = SingleSubject.create<List<Item>>()
    givenSearchResult("test", subject)
    val observer = store.stateChanges().test()
    store.dispatch(ListScreenAction.SubmitQuery("test"))

    subject.onError(IOException())

    observer.assertValueCount(3)
    observer.assertValueAt(2, ListScreenState(loading = false, query = "test", showRetry = true))
  }

  private fun givenSearchResult(query: String, single: Single<List<Item>>) {
    every { repository.search(query, "movie", "movie") }.returns(single)
  }

  private fun createItem(): Item {
    return Item("trackId", "trackName", null, null, null)
  }
}
