package com.dkostyrev.itunesbrowser.main

import com.dkostyrev.itunesbrowser.details.DetailsScreenState
import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.list.ListScreenAction
import com.dkostyrev.itunesbrowser.list.ListScreenState
import com.dkostyrev.itunesbrowser.list.ListScreenStore
import com.dkostyrev.itunesbrowser.repository.ItemsRepository
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.SingleSubject
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainScreenStoreTest {

  private val repository = mockk<ItemsRepository>()
  private val listScreenStore = ListScreenStore(repository)
  private val store = MainScreenStore(listScreenStore)

  private var disposable: Disposable? = null

  @Before
  fun setUp() {
    disposable = store.subscribe()
  }

  @After
  fun tearDown() {
    disposable?.dispose()
  }

  @Test
  fun `stateChanges - emits default state`() {
    val observer = store.stateChanges().test()

    observer.assertValueCount(1)
    observer.assertValue(MainScreenState(currentScreen = ItunesBrowserScreen.ListScreen(ListScreenState())))
  }

  @Test
  fun `OpenDetails - changes screen to details`() {
    val observer = store.stateChanges().test()
    val item = createItem()

    store.dispatch(MainScreenAction.OpenDetails(item))

    observer.assertValueCount(2)
    observer.assertValueAt(1, MainScreenState(currentScreen = ItunesBrowserScreen.DetailsScreen(DetailsScreenState(item))))
  }

  @Test
  fun `OpenList - changes screen to list`() {
    val observer = store.stateChanges().test()
    val item = createItem()
    store.dispatch(MainScreenAction.OpenDetails(item))

    store.dispatch(MainScreenAction.OpenList)

    observer.assertValueCount(3)
    observer.assertValueAt(2, MainScreenState(currentScreen = ItunesBrowserScreen.ListScreen(ListScreenState())))
  }

  @Test
  fun `ListScreenStore state changes - changes state of ListScreen`() {
    val observer = store.stateChanges().test()
    every { repository.search(any(), any(), any()) }.returns(SingleSubject.create())
    listScreenStore.dispatch(ListScreenAction.SubmitQuery("test"))

    observer.assertValueCount(2)
    observer.assertValueAt(1, MainScreenState(currentScreen = ItunesBrowserScreen.ListScreen(ListScreenState(query = "test", loading = true))))
  }

  private fun createItem() = Item("track", "trackName", null, null, null)
}
