package com.dkostyrev.itunesbrowser.repository

import android.net.Uri
import com.dkostyrev.itunesbrowser.api.ItunesApi
import com.dkostyrev.itunesbrowser.dao.ItemEntity
import com.dkostyrev.itunesbrowser.dao.ItunesDao
import com.dkostyrev.itunesbrowser.dao.QueryEntity
import com.dkostyrev.itunesbrowser.domain.Item
import com.dkostyrev.itunesbrowser.domain.Result
import com.dkostyrev.itunesbrowser.util.TestSchedulers
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import org.junit.Test

class ItemsRepositoryTest {

  private val api = mockk<ItunesApi>()
  private val dao = mockk<ItunesDao>()
  private val repository = ItemsRepository(
    api = api,
    dao = dao,
    schedulers = TestSchedulers(),
  )

  @Test
  fun `search - returns empty result - query is empty`() {
    val observer = repository.search(query = "", mediaType = "movie", entity = "movie").test()

    observer.assertComplete()
    observer.assertValueCount(1)
    observer.assertValue(emptyList())
  }

  @Test
  fun `search - returns dao result - query is present in dao`() {
    givenSearchResult(term = "test", media = "movie", entity = "movie", items = emptyList())
    givenDaoQuery(
      queryId = 1, term = "test", media = "movie", entity = "movie", items = listOf(
        ItemEntity(
          id = 1,
          queryId = 1,
          trackId = "track-1",
          trackName = "trackName-1",
          shortDescription = "desc-1",
          longDescription = "long-desc-1",
          artwork = "art-1"
        ),
        ItemEntity(
          id = 2,
          queryId = 2,
          trackId = "track-2",
          trackName = "trackName-2",
          shortDescription = "desc-2",
          longDescription = "long-desc-2",
          artwork = "art-2"
        )
      )
    )

    val observer = repository.search(query = "test", mediaType = "movie", entity = "movie").test()

    observer.assertComplete()
    observer.assertValueCount(1)
    observer.assertValue(
      listOf(
        Item(
          trackId = "track-1",
          trackName = "trackName-1",
          shortDescription = "desc-1",
          longDescription = "long-desc-1",
          artwork = Uri.parse("art-1"),
        ),
        Item(
          trackId = "track-2",
          trackName = "trackName-2",
          shortDescription = "desc-2",
          longDescription = "long-desc-2",
          artwork = Uri.parse("art-2"),
        )
      )
    )
  }

  @Test
  fun `search - returns api result and inserts query into dao - query is not present in dao`() {
    givenInsertQueryResult(term = "test", media = "movie", entity = "movie", queryId = 2)
    givenInsertItemsResult(
      listOf(
        ItemEntity(
          queryId = 2,
          trackId = "track-1",
          trackName = "trackName-1",
          shortDescription = "desc-1",
          longDescription = "long-desc-1",
          artwork = "art-1",
        )
      )
    )
    givenQueryEntity(term = "test", media = "movie", entity = "movie", queryEntity = null)
    givenSearchResult(
      term = "test", media = "movie", entity = "movie", items = listOf(
        Item(
          trackId = "track-1",
          trackName = "trackName-1",
          shortDescription = "desc-1",
          longDescription = "long-desc-1",
          artwork = Uri.parse("art-1"),
        )
      )
    )

    val observer = repository.search(query = "test", mediaType = "movie", entity = "movie").test()

    observer.assertComplete()
    observer.assertValueCount(1)
    observer.assertValue(
      listOf(
        Item(
          trackId = "track-1",
          trackName = "trackName-1",
          shortDescription = "desc-1",
          longDescription = "long-desc-1",
          artwork = Uri.parse("art-1"),
        )
      )
    )
  }

  private fun givenSearchResult(term: String, media: String, entity: String, items: List<Item>) {
    every { api.search(term, media, entity, limit = 50) }.returns(Single.just(Result(items, items.size)))
  }

  private fun givenQueryEntity(term: String, media: String, entity: String, queryEntity: QueryEntity?) {
    every { dao.queryByTerm(term, media, entity) }.returns(queryEntity?.let { Maybe.just(it) } ?: Maybe.empty())
  }

  private fun givenInsertQueryResult(term: String, media: String, entity: String, queryId: Long) {
    every { dao.insertQuery(QueryEntity(term = term, media = media, entity = entity)) }.returns(queryId)
  }

  private fun givenInsertItemsResult(items: List<ItemEntity>) {
    every { dao.insertItems(items) }.returns(Unit)
  }

  private fun givenDaoQuery(queryId: Int, term: String, media: String, entity: String, items: List<ItemEntity>) {
    givenQueryEntity(term, media, entity, QueryEntity(queryId, term, media, entity))
    every { dao.itemsByQueryId(queryId) }.returns(Single.just(items))
  }
}
