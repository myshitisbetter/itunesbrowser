package com.dkostyrev.itunesbrowser.util

import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers as RxSchedulers

class TestSchedulers(
  override val io: Scheduler = RxSchedulers.trampoline(),
  override val computation: Scheduler = RxSchedulers.trampoline(),
  override val mainThread: Scheduler = RxSchedulers.trampoline(),
) : Schedulers
