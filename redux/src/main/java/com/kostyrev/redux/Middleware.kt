package com.kostyrev.redux

import io.reactivex.rxjava3.core.Observable

interface Middleware<State : Any, Action : Any, Effect : Any> {
  fun create(action: Action, state: State): Observable<Effect>?
}
