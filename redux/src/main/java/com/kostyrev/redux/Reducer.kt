package com.kostyrev.redux

interface Reducer<State : Any, in Effect : Any> {
  fun reduce(state: State, effect: Effect): State
}
