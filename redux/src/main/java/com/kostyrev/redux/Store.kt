package com.kostyrev.redux

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable

interface Store<State : Any, in Action : Any, Effect : Any> {
  fun dispatch(action: Action)

  fun getState(): State

  fun stateChanges(): Observable<State>

  fun subscribe(): Disposable
}
