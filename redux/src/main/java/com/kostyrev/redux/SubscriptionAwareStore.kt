package com.kostyrev.redux

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.exceptions.CompositeException
import io.reactivex.rxjava3.kotlin.plusAssign
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.atomic.AtomicBoolean

class SubscriptionAwareStore<State : Any, in Action : Any, Effect : Any>(
  private val reducer: Reducer<State, Effect>,
  private val middleware: List<Middleware<State, Action, Effect>>,
  initialState: State
) : Store<State, Action, Effect> {

  private val state = BehaviorSubject.createDefault(initialState)
  private val actions = PublishSubject.create<Action>()
  private val effects = PublishSubject.create<Effect>()

  override fun subscribe(): Disposable {
    val disposable = CompositeDisposable()
    val middlewareMap = DisposableMap<Middleware<State, Action, Effect>>()
    disposable += effects
      .withLatestFrom(state) { effect, state -> reducer.reduce(state, effect) }
      .subscribe { state.onNext(it) }

    disposable += middlewareMap

    disposable += actions
      .withLatestFrom(state) { action, state ->
        middleware.forEach { middleware ->
          middleware.create(action, state)?.let { observable ->
            middlewareMap.set(middleware, observable.subscribe {
              effects.onNext(it)
            })
          }
        }

      }
      .subscribe()

    return disposable
  }

  override fun dispatch(action: Action) {
    actions.onNext(action)
  }

  override fun stateChanges(): Observable<State> {
    return state
      .distinctUntilChanged()
      .hide()
  }

  override fun getState(): State {
    return state.value!!
  }

  private class DisposableMap<Key : Any> : Disposable {
    private val disposed = AtomicBoolean(false)
    private val lock = Any()
    private val map = HashMap<Key, Disposable>()

    fun set(key: Key, disposable: Disposable) {
      if (!isDisposed) {
        synchronized(lock) {
          if (isDisposed) {
            disposable.dispose()
            return
          }

          map.put(key, disposable)?.dispose()
        }
      }
    }

    override fun dispose() {
      if (disposed.compareAndSet(false, true)) {
        synchronized(lock) {
          dispose(ArrayList(map.values))
          map.clear()
        }
      }
    }

    override fun isDisposed(): Boolean {
      return disposed.get()
    }

    private fun dispose(collection: Collection<Disposable>) {
      val throwable = collection.mapNotNull { it.disposeSafe() }
      if (throwable.isNotEmpty()) {
        throw CompositeException(throwable)
      }
    }

    private fun Disposable.disposeSafe(): Throwable? {
      return try {
        dispose()
        null
      } catch (throwable: Throwable) {
        throwable
      }
    }
  }
}
