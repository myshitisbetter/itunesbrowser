package com.kostyrev.redux

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import org.junit.Assert
import org.junit.Test

class SubscriptionAwareStoreTest {

  @Test
  fun `create store - emits initial state`() {
    val initialState = TestState(value = 1)
    val store = createStore(
      state = initialState,
      reducer = doNothingReducer(),
      middleware = middleware { _, _ -> Observable.never() }
    )

    val observer = store.stateChanges().test()

    observer.assertValueCount(1)
    observer.assertValue(initialState)
  }

  @Test
  fun `action dispatched - reduces state with reducer`() {
    val store = createStore(
      reducer = reducer { testState, _ ->
        testState.copy(value = testState.value + 1)
      },
      middleware = middleware { _, _ ->
        Observable.just(MiddlewareTestEffect())
      },
      state = TestState(value = 0)
    )
    val observer = store.stateChanges().test()
    observer.values().clear()

    store.dispatch(TestAction())

    observer.assertValueCount(1)
    observer.assertValueAt(0) {
      it == TestState(value = 1)
    }
  }

  @Test
  fun `action dispatched - propagates state to middleware`() {
    val expectedState = TestState()
    val states = ArrayList<TestState>()
    val store = createStore(
      reducer = doNothingReducer(),
      middleware = middleware { _, state ->
        states += state
        Observable.just(MiddlewareTestEffect())
      },
      state = expectedState
    )

    store.dispatch(TestAction())

    Assert.assertEquals(1, states.size)
    Assert.assertEquals(expectedState, states.first())
  }

  @Test
  fun `action dispatched - propagates action to middleware`() {
    val expectedAction = TestAction()
    val actions = ArrayList<TestAction>()
    val store = createStore(
      reducer = doNothingReducer(),
      middleware = middleware { action, _ ->
        actions += action
        Observable.just(MiddlewareTestEffect())
      },
      state = TestState()
    )

    store.dispatch(expectedAction)

    Assert.assertEquals(1, actions.size)
    Assert.assertEquals(expectedAction, actions.first())
  }

  @Test
  fun `action dispatched - propagates effect from middleware`() {
    val expectedState = TestState(value = 1)
    val store = createStore(
      reducer { state, effect ->
        if (effect is MiddlewareTestEffect) {
          state.copy(value = 1)
        } else {
          state
        }
      },
      middleware = middleware { _, _ ->
        Observable.just(MiddlewareTestEffect())
      },
      state = TestState()
    )
    val observer = store.stateChanges().test()

    store.dispatch(TestAction())

    observer.assertValueCount(2)
    observer.assertValueAt(1) {
      it == expectedState
    }
  }

  @Test
  fun `action dispatched - calls every middleware for every action`() {
    val invocations = ArrayList<String>()

    val store = createStore(
      doNothingReducer(),
      middleware = listOf(
        middleware { action, _ ->
          invocations += "first ${action.index}"
          null
        },
        middleware { action, _ ->
          invocations += "second ${action.index}"
          null
        }
      ),
      state = TestState()
    )

    repeat(2) { store.dispatch(TestAction(it)) }

    Assert.assertEquals(listOf("first 0", "second 0", "first 1", "second 1"), invocations)
  }

  @Test
  fun `action dispatched - disposes middleware observable - middleware creates new observable for action`() {
    val effects = ArrayList<TestEffect>()
    val firstSubject = PublishSubject.create<TestEffect>()
    val secondSubject = PublishSubject.create<TestEffect>()
    val store = createStore(
      doNothingReducer { effects += it },
      middleware = middleware { action, _ ->
        if (action.index == 0) firstSubject else secondSubject
      },
      state = TestState()
    )

    store.dispatch(TestAction(index = 0))
    firstSubject.onNext(MiddlewareTestEffect(index = 0))
    firstSubject.onNext(MiddlewareTestEffect(index = 0))
    store.dispatch(TestAction(index = 1))
    firstSubject.onNext(MiddlewareTestEffect(index = 0))
    secondSubject.onNext(MiddlewareTestEffect(index = 1))

    Assert.assertEquals(listOf(MiddlewareTestEffect(index = 0), MiddlewareTestEffect(index = 0), MiddlewareTestEffect(index = 1)), effects)
  }

  @Test
  fun `action dispatched - does not dispose middleware observable - middleware skips action creation`() {
    val effects = ArrayList<TestEffect>()
    val firstSubject = PublishSubject.create<TestEffect>()
    val store = createStore(
      doNothingReducer { effects += it },
      middleware = middleware { action, _ ->
        if (action.index == 0) firstSubject else null
      },
      state = TestState()
    )

    store.dispatch(TestAction(index = 0))
    firstSubject.onNext(MiddlewareTestEffect(index = 0))
    store.dispatch(TestAction(index = 1))
    firstSubject.onNext(MiddlewareTestEffect(index = 0))

    Assert.assertEquals(listOf(MiddlewareTestEffect(index = 0), MiddlewareTestEffect(index = 0)), effects)
  }

  private interface TestEffect

  private data class MiddlewareTestEffect(val index: Int = 0) : TestEffect

  private data class TestAction(val index: Int = 0)

  private data class TestState(val value: Int = 0)

  private fun createStore(
    reducer: Reducer<TestState, TestEffect>,
    middleware: List<Middleware<TestState, TestAction, TestEffect>>,
    state: TestState = TestState()
  ): Store<TestState, TestAction, TestEffect> {
    return SubscriptionAwareStore(reducer, middleware, state).also { it.subscribe() }
  }


  private fun createStore(
    reducer: Reducer<TestState, TestEffect>,
    middleware: Middleware<TestState, TestAction, TestEffect>,
    state: TestState = TestState()
  ): Store<TestState, TestAction, TestEffect> {
    return SubscriptionAwareStore(reducer, listOf(middleware), state).also { it.subscribe() }
  }

  private inline fun doNothingReducer(crossinline function: (TestEffect) -> Unit = {}): Reducer<TestState, TestEffect> {
    return reducer { state, effect ->
      function(effect)
      state
    }
  }

  private inline fun reducer(crossinline function: (TestState, TestEffect) -> TestState): Reducer<TestState, TestEffect> {
    return object : Reducer<TestState, TestEffect> {

      override fun reduce(state: TestState, effect: TestEffect): TestState {
        return function(state, effect)
      }
    }
  }

  private inline fun middleware(crossinline function: (TestAction, TestState) -> Observable<TestEffect>?): Middleware<TestState, TestAction, TestEffect> {
    return object : Middleware<TestState, TestAction, TestEffect> {

      override fun create(action: TestAction, state: TestState): Observable<TestEffect>? {
        return function(action, state)
      }
    }
  }
}
